#include "freezenn.h"
//#include "filefreezenn.h"


/**
*freezenn.c 
*Neural net source file
*
*Needs to load and backup NN weight array
*Runs the data array through the NN
*Produces NN output / checks runType / back propagation
*
*/

/**
*function generateOutput
*
*Runs basic linear regression algorthim
*
*@param neuron a node pointer
*
*/
void generateOutput(node *neuron){
	//(*neuron).output = (*neuron).weight * (*neuron).input;
	(*neuron).output = (*neuron).weight * (float)(*neuron).input;
}

/**
*function getOutput
*
*basic getter function
*
*@param neuron a node pointer
*@return float floating point output
*/
float getOutput(node *neuron){
	return (*neuron).output;
}

/*
int getOutput(node *neuron){
	return (*neuron).output;
}
*/


/**
*function updateWeights
*
*Original weight update fuction
*Shifts weights by 1/5(.20) in expected direction if expected output not found 
*
*two classes (o) and (x) to be identified
*0(o) is expecting a negative output (output < 0)
*1(x) is expecting a postive output (output > 0)
*
*
*o >= output
*	update weights -= .20	
*
*x <= output
*	update weights += .20
*
*@param nerualNet outputTotal runType 
*@return none
*/
void updateWeights(node *neuralNet, float outputTotal, char runType ){
	int i = 0;
	//Update weights
	if(runType == '0' && outputTotal < 0.0f){
		printf("Output as expected\n");
	}
	else if((runType== '0' && outputTotal > 0.0f) || (runType == '0' && outputTotal == 0.0f)){
		printf("Output not as expected, update weights\n");
		for(i= 0; i< DATA_SIZE; ++i){
			if(neuralNet[i].input == 1){
				neuralNet[i].weight -= 0.20f;
			}
		}
		
		
	}
	
	if(runType == '1' && outputTotal > 0.0f){
		printf("Output as expected\n");
	}
	else if((runType == '1' && outputTotal < 0.0f) || (runType == '1' && outputTotal == 0.0f)){
		printf("Output not as expected, update weights\n");
		for(i= 0; i< DATA_SIZE; ++i){
			if(neuralNet[i].input == 1){
				neuralNet[i].weight += 0.20f;
			}
		}
	}
	
}


/**
*function updateWeightDelta
*
*Uses a perceptron weight update function (based on delta rule)
*
*w = w + r * (d - y) * x
*
*weight = weight + learning rate * (desired output - output) * input
*
*output is not total output it's output of the node
*
*/
void updateWeightDelta(node* neuralNet, float outputTotal, char runType){
	int i = 0;
	float lr = 1.0f; //learing rare
	float desiredOutput = 0.0f;
	float givenOutput = 0.0f;
	
	if(runType == '0'){
		desiredOutput = -1.0f;
	}
	else{
		desiredOutput = 1.0f;
	}
	
	//because this rule is the difference between input and output and derives the new weight from the difference
	//desired output is reduced down to a binary 1/-1 the output must also be reduced to (or between - 0 being indeterimnate between the two classes (x) and (o)) a number between 1/0/-1
	//having the linear regression output effect weights based on difference against -1 and 1 desired output doesn't work.
	if(outputTotal > 0.0){
		givenOutput = 1.0f;
	}
	
	//this if statement is uneccessary due to default value in given output being 0.0
	/*
	if(outputTotal == 0.0){
		givenOutput = 0.0f;
	}
	*/
	
	if(outputTotal < 0.0){
		givenOutput = -1.0f;
	}
	
	//technically if the desired output matches the given output there is no need to train the weights as they're "correct"
	//only mismatched training files require weights to be adjusted.
	//the algorithm itself won't update weights that match desired output. 
	
	printf("Update weights for desired output %.2f\n", desiredOutput);
	
	for(i = 0; i < DATA_SIZE; i++){
		//printf("Before node %d  weight = %.2f, lr= %.2f desiredOutput = %.2f, given total = %.2f, neural net Input = %d\n", i, neuralNet[i].weight, lr, desiredOutput, givenOutput, neuralNet[i].input);
		neuralNet[i].weight = neuralNet[i].weight + lr * (desiredOutput - givenOutput) * (float)neuralNet[i].input;
		//printf("After node %d  weight = %.2f, lr= %.2f desiredOutput = %.2f, given total = %.2f, neural net Input = %d\n", i, neuralNet[i].weight, lr, desiredOutput, givenOutput, neuralNet[i].input);
	}
	
	//original wont work unless desired output matches the total input pixels and inverted for (o) image
	/*
	for(i = 0; i < DATA_SIZE; i++){
		printf("Before node %d  weight = %.2f, lr= %d desiredOutput = %d, output total = %.2f, neural net Input = %d\n", i, neuralNet[i].weight, lr, desiredOutput, outputTotal, neuralNet[i].input);
		neuralNet[i].weight = neuralNet[i].weight + lr * (desiredOutput - outputTotal) * neuralNet[i].input;
		printf("After node %d  weight = %.2f, lr= %d desiredOutput = %d, output total = %.2f, neural net Input = %d\n", i, neuralNet[i].weight, lr, desiredOutput, outputTotal, neuralNet[i].input);
	}
    */
}






/**
*function runNN
*
*"Runner" function for NN
*Runs input into nodes(neurons)
*Runs algorithm in nodes and produces output
*Coallates output
*linearRegression algorithm lr = C(oefficient) + W(eight) * I(input) + W2 * I2 ...
*
*backpropagation?
*
*@param dataArray unsigned char pointer to data array processed pixel data
*@param runType running mode
*/
void runNN(unsigned char *dataArray, char runType){
	node *neuralNet = NULL;
	int i = 0;
	float outputTotal = 0.0f;
	//if(runType != '0' && runType != '1' && runType != '2')
	//	printf("runtype not equal to 0,1 or 2\n");
	
	
	//if((sizeof(dataArray)/sizeof(dataArray[0])) != DATASIZE)
	//{
	//printf("datasize mismatch %d %d %d\n",sizeof(dataArray), sizeof(dataArray[0]), DATASIZE);
	//}
	//check inputData
	if(dataArray == NULL || (runType != '0' && runType != '1' && runType != '2')){
		printf("Error: neural net data verification error\n");
		return;
	}

	//load neural net from file or original
	neuralNet = loadNN();

	if(neuralNet == NULL){
		printf("Error: Neural Net failed to load or register\n");
		return;
	}
	
	//clear outputs
	for(i = 0; i < DATA_SIZE; ++i){
		neuralNet[i].output = 0.0f;
	}
	
	
	//have a loadedNN;
	//run insert inputs
	for(i = 0; i < DATA_SIZE; ++i){
		neuralNet[i].input = dataArray[i];
		//printf("node output file = %d\n %d %d %d\n", i, neuralNet[i].input, neuralNet[i].weight, neuralNet[i].output);
		//printf("node output file = %d\n %d %f %f\n", i, neuralNet[i].input, neuralNet[i].weight, neuralNet[i].output);
	}
	
	for(i = 0; i < DATA_SIZE; ++i){
		generateOutput(&neuralNet[i]);
		//printf("node output file = %d\n %d %d %d\n", i, neuralNet[i].input, neuralNet[i].weight, neuralNet[i].output);
		//printf("node output file = %d\n %d %f %f\n", i, neuralNet[i].input, neuralNet[i].weight, neuralNet[i].output);
	}
	
	for(i = 0; i < DATA_SIZE; ++i){
		outputTotal += getOutput(&neuralNet[i]);
		//printf("node output file = %d\n %d %d %d\n", i, neuralNet[i].input, neuralNet[i].weight, neuralNet[i].output);
		//printf("node output file = %d\n %d %f %f\n", i, neuralNet[i].input, neuralNet[i].weight, neuralNet[i].output);
	}
	
	//printf("%c\n", runType);
	
	//output
	if(outputTotal == 0.0f){
		printf("Prediction image is neither X or 0, outputTotal = %.2f\n", outputTotal);
	}
	else if(outputTotal > 0.0f){
		printf("Prediction X image, outputTotal = %.2f \n", outputTotal);
	}
	else if(outputTotal < 0.0f){
		printf("Prediction 0 image, outputTotal = %.2f\n", outputTotal);
	}
	
	//if not a prediction run update weights
	if(runType != '2')
		updateWeightDelta(neuralNet, outputTotal, runType);
	
	//comment out above and uncomment this block to use linear shifting weights
	/*
	if(runType != '2')
		updateWeights(neuralNet, outputTotal, runType);
	*/
	
	
	//backup NN
	backupNN(neuralNet);
	
	free(neuralNet);
}

/**
*function batchTrain
*
*loads batch file information from setup.txt file and trains the NN.
*
*/
void batchTrain(void){
	//must run the nn for as many times as in batchfile
	unsigned int files = 0;
	unsigned int counter = 0;
	char buffer[1000] = "";
	char dummy, runType;
	char buffer2[25] = "";
	char filename[LOCATION_SIZE + 25];
	unsigned char *pixelArray =  NULL;
	
	
	//failed to copy batch location
	/*
	if(filename == NULL) {
		printf("Error: string concat failed with batch directory\n");
		return;
	}
	*/
	
	if(checkFile(BATCH_FILENAME)){
		
		FILE *filePtr = fopen(BATCH_FILENAME, "r"); //open file for reading
		
		if(filePtr!= NULL){
			fgets(buffer, 1000, filePtr); //reads setup information
			sscanf(buffer,"%d", &files);
			
			for(counter = 0; counter < files; ++counter){
				memset(buffer, '\0', 1000 * sizeof(char));  //reset memory
				memset(buffer2, '\0', 50 * sizeof(char));
				memset(filename, '\0', LOCATION_SIZE+25 * sizeof(char));
				strncpy(filename, BATCH_LOCATION, LOCATION_SIZE);
				//printf("filename before cat = %s\n", filename);
				fgets(buffer, 1000, filePtr);
				sscanf(buffer, "%c%c%s", &runType, &dummy, buffer2);
				strncat(filename, buffer2, 25);
				
				//printf("runType = %c, filename = %s\n", runType, filename);
				
				pixelArray = getBMPData(filename); //pixel imputs feed into basic nn
			
				if(pixelArray != NULL){
					runNN(pixelArray, runType); //run prediction on image filename
					free(pixelArray);
				}
			}
			
			
		}
		
		
	}
	else{
		printf("ERROR: Batch file doesn't exist at location %s\n", BATCH_FILENAME);
	}
	
	
}
