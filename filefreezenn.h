#ifndef FILEFREEZENN_H
#define FILEFREEZENN_H

#include "freezeglobal.h"

bool checkFile(char *filename);  //checks valid file
unsigned char * getBMPData(char *filename);  //parses BMP data
node * loadNN(void);  //load or creates neural net file
void backupNN(node *); //back up neural net
bool loadSetup(char *filename); //load setup file
node * generateNN(void); //generates a NN from a template
void printNN(void); //output NN
void printNNW(void); //outputs NN weights by image row
#endif