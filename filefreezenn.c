#include "filefreezenn.h"

/**
*filefreezenn.c
*
*Handles file manipulation/checking, BMP image file parsing, Neural Net output
*/

//may not be able to get away with this
/*
node baseNN[100] = {{0,1,0},{0,0,0},{0,-1,0},{0,-1,0},{0,-1,0},{0,-1,0},{0,-1,0},{0,-1,0},{0,-1,0},{0,1,0},
					{0,-1,0},{0,1,0},{0,0,0},{0,0,0},{0,0,0},{0,0,0},{0,0,0},{0,0,0},{0,1,0},{0,-1,0},
					{0,-1,0},{0,0,0},{0,1,0},{0,0,0},{0,0,0},{0,0,0},{0,0,0},{0,1,0},{0,0,0},{0,-1,0},
					{0,-1,0},{0,0,0},{0,0,0},{0,1,0},{0,0,0},{0,0,0},{0,1,0},{0,0,0},{0,0,0},{0,-1,0},
					{0,-1,0},{0,0,0},{0,0,0},{0,0,0},{0,1,0},{0,1,0},{0,0,0},{0,0,0},{0,0,0},{0,-1,0},
					{0,-1,0},{0,0,0},{0,0,0},{0,0,0},{0,1,0},{0,1,0},{0,0,0},{0,0,0},{0,0,0},{0,-1,0},
					{0,-1,0},{0,0,0},{0,0,0},{0,1,0},{0,0,0},{0,0,0},{0,1,0},{0,0,0},{0,0,0},{0,-1,0},
					{0,-1,0},{0,0,0},{0,1,0},{0,0,0},{0,0,0},{0,0,0},{0,0,0},{0,1,0},{0,0,0},{0,-1,0},
					{0,-1,0},{0,1,0},{0,0,0},{0,0,0},{0,0,0},{0,0,0},{0,0,0},{0,0,0},{0,1,0},{0,-1,0},
					{0,1,0},{0,-1,0},{0,-1,0},{0,-1,0},{0,-1,0},{0,-1,0},{0,-1,0},{0,-1,0},{0,-1,0},{0,1,0}};
*/
/*
node baseNN[100] = {{0.0,1.0,0.0},{0.0,0.0,0.0},{0.0,-1.0,0.0},{0.0,-1.0,0.0},{0.0,-1,0},{0.0,-1.0,0.0},{0.0,-1.0,0.0},{0.0,-1.0,0},{0.0,-1.0,0.0},{0.0,1.0,0.0},
					{0.0,-1.0,0.0},{0.0,1.0,0.0},{0.0,0.0,0.0},{0.0,0.0,0.0},{0.0,0.0,0.0},{0.0,0.0,0.0},{0.0,0.0,0.0},{0.0,0.0,0.0},{0.0,1.0,0.0},{0.0,-1.0,0.0},
					{0.0,-1.0,0.0},{0.0,0.0,0.0},{0.0,1.0,0.0},{0.0,0.0,0.0},{0.0,0.0,0.0},{0.0,0.0,0.0},{0.0,0.0,0.0},{0.0,1.0,0.0},{0.0,0.0,0.0},{0.0,-1.0,0.0},
					{0.0,-1.0,0.0},{0.0,0.0,0.0},{0.0,0.0,0.0},{0.0,1.0,0.0},{0.0,0.0,0.0},{0.0,0.0,0.0},{0.0,1.0,0.0},{0.0,0.0,0.0},{0.0,0.0,0.0},{0.0,-1.0,0.0},
					{0.0,-1.0,0.0},{0.0,0.0,0.0},{0.0,0.0,0.0},{0.0,0.0,0.0},{0.0,1.0,0.0},{0.0,1.0,0.0},{0.0,0.0,0.0},{0.0,0.0,0.0},{0.0,0.0,0.0},{0.0,-1.0,0.0},
					{0.0,-1.0,0.0},{0.0,0.0,0.0},{0.0,0.0,0.0},{0.0,0.0,0.0},{0.0,1.0,0.0},{0.0,1.0,0.0},{0.0,0.0,0.0},{0.0,0.0,0.0},{0.0,0.0,0.0},{0.0,-1.0,0.0},
					{0.0,-1.0,0.0},{0.0,0.0,0.0},{0.0,0.0,0.0},{0.0,1,0.0},{0.0,0.0,0.0},{0.0,0.0,0.0},{0.0,1.0,0.0},{0.0,0.0,0.0},{0.0,0.0,0.0},{0.0,-1.0,0.0},
					{0.0,-1.0,0.0},{0.0,0.0,0.0},{0.0,1.0,0.0},{0.0,0,0.0},{0.0,0.0,0.0},{0.0,0.0,0.0},{0.0,0.0,0.0},{0.0,1.0,0.0},{0.0,0.0,0.0},{0.0,-1.0,0.0},
					{0.0,-1.0,0.0},{0.0,1.0,0.0},{0.0,0.0,0.0},{0.0,0,0.0},{0.0,0.0,0.0},{0.0,0.0,0.0},{0.0,0.0,0.0},{0.0,0.0,0.0},{0.0,1.0,0.0},{0.0,-1.0,0.0},
					{0.0,1.0,0.0},{0.0,-1.0,0.0},{0.0,-1.0,0.0},{0.0,-1,0.0},{0.0,-1.0,0.0},{0.0,-1.0,0.0},{0.0,-1.0,0.0},{0.0,-1.0,0.0},{0.0,-1.0,0.0},{0.0,1.0,0.0}};
*/

//node template = {0,0,0};
node template = {0.0,0.0,0.0}; //a single node for initialization

					
					
					
					
//returns true if the file name exists
/**
*function checkFile
*
*Checks the existance of file "filename" by opening the file successfully
*returns bool type true(1) or false(0)
*
*@param filename char *
*@return bool true/false
*/
bool checkFile(char* filename){
	bool value = false;
	//open binary file stream
	FILE *filePtr = fopen(filename, "rb");
	
	if( filePtr != NULL){
		fclose(filePtr);
		return (value = true);
	}
		
	return value;
}

/**
*function generateNN
*
*generates an empty neural network and returns the address
*
*@return node* returns a clean neural network
*/
node * generateNN(void){
	int i = 0;
	node *nodePtr = malloc(sizeof(node) * DATA_SIZE);
	
	for(i = 0; i < DATA_SIZE; ++i){
		nodePtr[i].input = template.input;
		nodePtr[i].weight = template.weight;
		nodePtr[i].output = template.output;
	}
		
	return nodePtr;	
}


/**
*function loadSetup
*
*loads the setup information from the setup.txt file
*
*@param filename char *
*@return bool success true or false
*/
bool loadSetup(char *filename){
	bool value = false;
	char buffer[1000] = ""; //1000 char read from text file
	char dummy = ' ';
	unsigned int nnSize, batchSize; //unsigned ints for string malloc
	
	//NN_FILENAME = malloc(sizeof(char) * 25);
	//NN_FILENAME[0] = ' ';
	
	FILE *filePtr = fopen(filename, "r");

	fgets(buffer, 1000, filePtr);  //skip first line
	memset(buffer, '\0', 1000);  //reset memory
	fgets(buffer, 1000, filePtr); //reads setup information
	
	//printf("%s\n", buffer);
	//parse
	//sscanf(buffer,"%d%c%d%c%s", &PIXEL_WIDTH, &dummy, &PIXEL_HEIGHT, &dummy, NN_FILENAME);
	sscanf(buffer,"%d%c%d%c%d%c%d%c%d", &PIXEL_WIDTH, &dummy, &PIXEL_HEIGHT, &dummy, &nnSize, &dummy, &LOCATION_SIZE, &dummy, &batchSize);
	NN_FILENAME = malloc(sizeof(char) * nnSize);
	BATCH_LOCATION = malloc(sizeof(char) * LOCATION_SIZE);
	BATCH_FILENAME = malloc(sizeof(char) * batchSize);
	
	//if failed to malloc size
	if(NN_FILENAME == NULL || BATCH_LOCATION == NULL || BATCH_FILENAME == NULL)
		return value;
	
	memset(buffer, '\0', 1000 * sizeof(char));  //reset memory
	fgets(buffer, 1000, filePtr); //reads setup information
	sscanf(buffer,"%s", NN_FILENAME);
	
	memset(buffer, '\0', 1000 * sizeof(char));  //reset memory
	fgets(buffer, 1000, filePtr); //reads setup information
	sscanf(buffer,"%s", BATCH_LOCATION);
	
	memset(buffer, '\0', 1000 * sizeof(char));  //reset memory
	fgets(buffer, 1000, filePtr); //reads setup information
	sscanf(buffer,"%s", BATCH_FILENAME);
	
	//printf("%d px width, %d px height, NN filename = %s, Batch location = %s, Batch filename= %s\n", PIXEL_WIDTH, PIXEL_HEIGHT, NN_FILENAME, BATCH_LOCATION, BATCH_FILENAME);
	
	if(PIXEL_WIDTH > 0 && PIXEL_WIDTH > 0 && NN_FILENAME[0] != ' '){
		DATA_SIZE = PIXEL_WIDTH * PIXEL_HEIGHT;
		printf("Successfully loaded setup file\n");
		return (value = true);
	}
		
		
	return value;
}




/**
*function getBMPData
*
*Opens, checks and parses 10x10 pixel BMP file for pixel information
*can be used for direct data file return 
*Mallocs data onto the heap.
*Data used as input nodes into NN.
*
*@param filename char *
*@return uchar* byte array of parsed BMP monochrome pixel data
*/
unsigned char * getBMPData(char *filename){
	unsigned char fileHeader[2] = {' '};
	int pixelOffset = 0;
	int fileSize = 0;
	int fileSizeCounter = 0;
	unsigned char dummyChar = ' ';
	unsigned char *data = NULL;
	//int pixelData[10] = {0}; //pixel data is determined by height
	int *pixelData = malloc(sizeof(int) * PIXEL_HEIGHT); //pixel data is determined by height
	//counter for loop
	int i = 0;
	int d = 0;
	int bitmask = 1;
	//bit flags
	//int flag = 1 << 31;
	
	
	FILE *fptr = fopen(filename, "rb");
	//check if the file is BMP
	//extract file size information
	//extra pixels
	//monochrome BMP image has 1 bit information stored over 4 bytes LittleEndian
	
	//check pixelData array
	if(pixelData == NULL || fptr == NULL)
		return NULL;
	
	
	//clear malloced address space
	memset (pixelData, 0, sizeof (int) * PIXEL_HEIGHT);
	
	//for(i =0; i < PIXEL_HEIGHT; ++i){
	//	pixelData[i] = 0;
	//	printf("pixel data %d\n", pixelData[i]);
	//}
	
	fread(&fileHeader, sizeof(unsigned char), 2, fptr);
	
	//if file header doesn't match return null
	if(strncmp((const char *)fileHeader, "BM" ,2)  != 0)
	{
		printf("Error: Image file is not BMP format.\n");
		return NULL;
	}
	
	fread(&fileSize, sizeof(int), 1, fptr);
	//printf("file size read %d\n", fileSize);
	
	//reset file index to start
	fseek(fptr, 0, SEEK_SET);
	//count file size /not efficent.
	while(!feof(fptr)){
		fread(&dummyChar, sizeof(unsigned char), 1, fptr);
		++fileSizeCounter; //count file size in bytes.
	}
	
	//printf("file size counter %d\n", fileSizeCounter);
	
	//if file size doesn't match file byte count, file is damaged.
	if(fileSize != fileSizeCounter-1){
		printf("Error: BMP image file size mismatch, file corrupted.\n");
		return NULL;
	}
	
	//SEEK 10 bytes from the beginning for pixel offset
	fseek(fptr, 10, SEEK_SET);
	//read the pixel offset off header
	fread(&pixelOffset, sizeof(int), 1, fptr);
	
	//move to pixelOffset
	fseek(fptr, pixelOffset, SEEK_SET);
	//BMP pixel data is stored in 32-bit DWORD (4x8bit bytes)
	//10pixel data in monochrome is stored in 1 bit (1 or 0)
	
	//fread(pixelData, sizeof(int), 10, fptr);
	for(i = 0; i < PIXEL_HEIGHT; ++i){
		for(d = 0; d < 2; ++d){
			fread(&dummyChar, sizeof(unsigned char), 1, fptr);
			//printf("dummy byte = %d  dValue = %d\n", dummyChar, d);
			pixelData[i] += dummyChar;
			if(d==0)
				pixelData[i] <<= 8; //shift 8bits add second byte
			
			dummyChar = '0';
			//printf("pixel data%d %d\n", i, pixelData[i]);
		}
		
		pixelData[i] >>= 6; //trim 6bits for 10bit output 10px output
		//printf("trimmed pixel data at row %d %d\n\n", i, pixelData[i]);
		fseek(fptr, 2, SEEK_CUR);
	}
	
	
	
	//turn pixel data into input data
	data = malloc(sizeof(unsigned char) * DATA_SIZE);
	//process bit data into NN input bit data
	//FIX HERE
	
	for(i = 0; i < PIXEL_HEIGHT; ++i){
		pixelData[i] =~ pixelData[i]; //flips bits, ~ bit compliment operator because of bmp monochrome
		//4 byte int needs to be shifted PIXEL bits
		//foreach bit in the pixelData[i] 4 byte word shift to input data (may need to manipulate)
		for(d = 0; d < PIXEL_WIDTH; ++d){
			data[i*PIXEL_HEIGHT+d] = ((pixelData[i] >> d) & bitmask);
		}
		//printf("pixelData[%d] %d\n", i, pixelData[i]);	
	}

	
	
	//OLD function for processing bit stream
	/*
	for(i = 0; i < PIXEL_HEIGHT; ++i){
		//4 byte int needs to be shifted for bits
		//foreach bit in the pixelData[i] 4 byte word shift to input data (may need to manipulate)
		for(d = 0; d < PIXEL_WIDTH; ++d){
			if((pixelData[i] >> d) & bitmask){ //shift bits right d times and check against bitmask, if the "and" op successful store 0 (whitespace) else store 1(blackspace) / flips monochrome BMP pixel)
				//printf("bit and success %d at pixel %d\n", pixelData[i] >> d, d);	
				data[i*PIXEL_HEIGHT+d] = 0;
			}
			else{
				//printf("bit and failed %d at pixel %d\n", pixelData[i] >> d, d);
				data[i*PIXEL_HEIGHT+d] = 1;
			}
		}
		//printf("pixelData[%d] %d\n", i, pixelData[i]);	
	}
	*/
	
	//check data array
	/*
	for(i = 0; i< PIXEL_HEIGHT*PIXEL_WIDTH; ++i){
		printf("data=%d  at i=%d\n", data[i], i);
	}
	*/
	
	free(pixelData);
	
	fclose(fptr);
	
	return data;
	
	
}


/**
*function loadNN
*
*Loads existing NN from datafile or creates a new file 
*using original dummy data contained in freezeglobal.h
*
*@return node* returns NN either generated or existing from file
*
*/

node * loadNN(void){
	
	if(checkFile(NN_FILENAME)){
		FILE *fptr = fopen(NN_FILENAME, "rb");
		//create and read from existing NN
		node *nodePtr = malloc(sizeof(node) * DATA_SIZE);
		
		if(nodePtr == NULL)
			return NULL;
		
		fread(nodePtr, sizeof(node), DATA_SIZE, fptr);
		
		fclose(fptr);
		
		printf("Loading existing Neural Net\n");
		
		return nodePtr;
		
	}
	else{
		FILE *fptr = fopen(NN_FILENAME, "wb+");
		
		node *nodePtr = generateNN();
		
		if(fptr == NULL)
			return NULL;
		
		printf("Neural Net file freezeNN.dat not found, creating file from original data (untrained)\n");
		
		//create and write base neural network and close file
		
		//fwrite(baseNN, sizeof(node), 100, fptr);
		fwrite(nodePtr, sizeof(node), DATA_SIZE, fptr);
			
		fclose(fptr);
			
		//return dummy array from freezeglobal.h
		return nodePtr;	
	}
	//check if file exists
	//if it does load nodeData
	//if not load base NN
	//return data
}


/**
*function backupNN
*
*backup neural net to file / updates neural net
*
*@param neuralNet pointer to neural net data type
*/
void backupNN(node *neuralNet){
	FILE *fptr = fopen(NN_FILENAME, "wb+");
		
		if(fptr == NULL){
			printf("Error updating/writing neural network file\n");
			return;
		}
		
		//printf("Neural Net file freezeNN.dat not found, creating file from original data (untrained)\n");
		//create and write base neural network and close file
		fwrite(neuralNet, sizeof(node), DATA_SIZE, fptr);
			
		fclose(fptr);
					
}

/**
*function printNN
*
*outputs the neural net to stdout
*
*/
void printNN(void){
	node *outputNN = loadNN();
	int i = 0;
	
	for(i = 0; i < DATA_SIZE; ++i){
		printf("Neuron address %d\n", i);
		printf("Neural Net last input %d\n", outputNN[i].input);
		printf("Neural Net last weight %.2f\n", outputNN[i].weight);
		printf("Neural Net last output %.2f\n\n", outputNN[i].output);
	}
	
	free(outputNN);
	
}

/**
*function printNN
*
*outputs the neural net weights to stdout and split on image width rows
*
*/
void printNNW(void){
	node *outputNN = loadNN();
	int i = 0;
	
	for(i = 0; i < DATA_SIZE; ++i){
		
		printf("%.2f", outputNN[i].weight);
		
		if((i+1)%PIXEL_WIDTH == 0){
			printf("\n");
		}
		else{
			printf(",");
		}
	
	}
	
	free(outputNN);
	
}

