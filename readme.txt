Freeze Neural Network readme file

Written and Developed by David Clarke 2018.

1.0.0 Quickstart
    1.1.0 Basic Usage
	1.2.0 Usage
	    1.2.1 Help Dialog
		1.2.2 Prediction
		1.2.3 Training (Single file)
		1.2.4 Print Neural Network
		1.2.5 Print Neural Network Image Width (rows)
		1.2.6 Batch Training
2.0.0 Introduction
3.0.0 Setup
    3.1.0 Directory Structure (compiled version)
	3.2.0 setup.txt
	3.3.0 trainbatch.txt
4.0.0 Freeze Neural Network
    4.1.0 Input
    4.2.0 Weighted Algorithm
    4.3.0 Output
    4.4.0 Adjusted Weights
5.0.0 Conclusion
    5.1.0 Improvements
	5.2.0 Programming extensions
	5.3.0 Future work
6.0.0 Appendix

1.0.0 Quickstart

1.1.0 Basic Usage

A sample usage of using the Freeze Neural Network (FNN)

freeze batch
freeze print
freeze xfile1.bmp

The first command trains the current neural network
The second command prints the contents of the neural network (weights)
The third command tries to predict the content of xfile1.bmp

1.2.0 Usage
1.2.1 Help Dialog (Prints basic help dialog)
	$/programName
	or
	$/programName -h

	example 
	    $/freeze
	    or
	    $/freeze -h

1.2.2 Prediction (Runs NN prediction on a single monochrome BMP file)
	$/programName fileName.bmp
	
	example
	    $/freeze xfile1.bmp
		or
		$/freeze ofile1.bmp
	
1.2.3 Training (Single file)
    $/programName fileName.bmp expected_outcome
	
	example
	    $/freeze xfile1.bmp 1
		or
		$/freeze ofile1.bmp 0
		
	Expected outcome flag is 0 for O shape and 1 for X shape image.	
	*Warning* Expected outcome flag must match image type or training will fail or at least will be backwards
	
1.2.4 Print Neural Network
    $/programName print
	
	example
	    $/freeze print
		
1.2.5 Print Neural Network weight output based on image rows
    $/programName printw
	
	example
	    $/freeze printw
		
1.2.6 Batch Training (multiple files based on setup file)
	$/programName batch
	
	example
	    $/freeze batch
	
2.0.0 Introduction

After completing the University of Helsinki's: Elements of AI https://www.elementsofai.com/ free online course, I decided to implement a basic neural network, Freeze(FreezeNN), based on their example of an image recognition neural network.

An Artificial Neural Netwok(ANN) is the computerized analogy of a biological neural network. Neurons take an input signal, react to the input signal and produce an output signal; output can be fed into more neurons for further processing.
The power of this processing comes from the ability of neurons to strengthen or reinforce their response, to create new links and form meshes and many other biological factors that are still not fully known. 
ANN's try to mimic this ability.

The history and an example of ANN can be read here: https://en.wikipedia.org/wiki/Artificial_neural_network
The description of the biological neuron can be found here: https://en.wikipedia.org/wiki/Neuron

This program seeks to predict a basic image class and determine, based on its learning, what the image shows. I've constrained the AI to be as basic as possible to allow for rapid development, while not only successfully implement
the Elements of AI neural network example, but also include a learning algorithm and batch training.

Because the ANN is single layer and uses linear regression the neural network is analogous to a Perceptron ANN.  https://en.wikipedia.org/wiki/Perceptron

The Freeze ANN takes a 10 pixel by 10 pixel monochrome Bitmap(BMP) image file (an X-image or an O-image) as input, processes the input into a weighted layer(array) and uses the linear regression algorithm to process an output. The output of the neural network is the sum total of weighted input.
If the single file training or batch commands are used, the NN layer's weights are updated after processing.

To use the program successfully please read the setup guide and the Freeze Neural Network section.

3.0.0 Setup

Setting up the FNN should include checking the file structure of the program, the setup files (setup.txt/trainbatch.txt) and making sure the batch directory contents match the trainbatch.txt contents.

3.1.0 Directory Structure (compiled version):

-/main directory
   Freeze
   xfile1.bmp
   xfile2.bmp
   ofile1.bmp
   ofile2.bmp
   setup.txt
   --/train directory
       trainbatch.txt
	   xfile1.bmp
	   ofile2.bmp
	   xfile3.bmp
	   ofile4.bmp
	   xfile5.bmp
	   ofile6.bmp
	   xfile7.bmp
	   ofile8.bmp
	   xfile9.bmp
	   ofile10.bmp
	   etc
	   
As described above, the batch directory and training files are split into a TRAIN sub-directory and processing is done using trainbatch.txt for number of files and filenames to process. The xfiles and ofiles in the main directory are used for testing prediction.

3.2.0 setup.txt 

Setup.txt allow users to change the size of the neural network via image pixel width and height (which means you can processs any size pixel image (BMP monochrome))
the other details are the sizes and then names on each line: the neural network(default:freezeNN.dat) and then the batch training directory(default:train) and batch filename (default:trainbatch.txt)

--EXAMPLE SETUP FILE----------------------------------------------------------------------
pixel width;pixel height;neural net filename size;batch location size;batch filename size
10;10;12;6;20
freezeNN.dat
train\
train\trainbatch.txt
------------------------------------------------------------------------------------------

The first line of setup.txt describes the following line
The second line is image pixel width, image pixel height, the neural network filename's size (number of characters), training directory size (number of characters) and trainbatch.txt location size (number of characters).
The third line is the name of the neural network saved and/or to be loaded on disk.
The fourth line is the training directory name
The fifth line is the trainbatch.txt filename and location.

the ';'(semicolon) character is used as a character delimiter

When you change locations of the train directory or filenames you must change the name sizes on the second line of the setup file.

freezeNN.dat is a binary file (use a hex editor to read it), NN stored as (int, float, float)
setup.txt and trainbatch.txt are both human readable text.

The setup.txt name is hard-coded into the freezeglobal.h on line 8

3.3.0 trainbatch.txt

This is batch training runner file which has the format:

number_of_files
expected_outcome;filename1
expected_outcome;filename2
expected_outcome;filename3
expected_outcome;filename4
expected_outcome;filename5
expected_outcome;filename6

--EXAMPLE TRAINBATCH FILE EXTRACT-------------------------------
4
1;xfile1.bmp
0;ofile2.bmp
1;xfile3.bmp
0;ofile4.bmp
--------------------------------------------------------

When training freezeNN the expected outcome comes in two options: 1 for x-image files, and 0 for o-image files.
Wrong input regarding expected outcome and the content of the image file will either train the NN backwards (technically not wrong if interpreted correctly, considering it is a binary option) or otherwise leads to incorrect training.

I have noticed when developing the program that stacking x-images one after the other and then training only o-images usually over-weighs certain neurons, training the NN in turn creates a more balanced network.
This conclusion is only from initial training results based on an earlier linear weighting and not from extensive testing of the final NN.

4.0.0 Freeze Neural Network

The Freeze Neural Network(FreezeNN) predicts the subject of an image file. 

The inputs into the neural network are X-images and o-images in a 10 pixel by 10 pixel bitmap monochrome image. 
Technically, if you adjust the setup file to accept a larger pixel width and height, you can have any size monochrome (black/white) bitmap images. (untested)

The FreezeNN automatically creates a weightless dummy neural network if one doesn't exist already.

Freeze can be trained one BMP image file at a time with an expected outcome using the command line. It can also be trained using batch processing for large training sets.

The FreezeNN can output the Neural Network using the print command.

The FreezeNN is a Perceptron Neural Network that updates its single layer neuron weights during training runs (single and batch). It's an example of a supervised learning single layer neural network.

This network and its function was described in detail in the University of Helsinki's: Elements of AI course. After completing this course I decided to implement the neural network in C to further my learning.

Basic Function
input array -> weighted algorithm -> output -> adjust weights(*)

Weights are only adjusted on training runs, otherwise it only predicts input against the current NN.

4.1.0 Input

Input in the FreezeNN comes in the form of monochrome bitmap images (10 pixel width, 10 pixel height) these images are parsed and the pixel data (10x10 bits stored pixels) and rationalized into the data input array.

This parsed data array is subsequently loaded into the neural network array nodes (neurons).

Function in filefreezenn.c:
unsigned char * getBMPData(char *filename)

The BMP file format can be found here: https://en.wikipedia.org/wiki/BMP_file_format

The monochrome BMP file was chosen because it's unencoded, simple format and easily created (MSPaint/GIMP).

Note: The BMP packs the data bottom to top, I didn't flip the data around in the program, this isn't harmful to the network but it is notable. Xs and Os are natural mirrors.
Note2: 6 test and 100 training BMP files have been made available for testing.


4.2.0 Weighted Algorithm

The neurons in FreezeNN are described by this c structure stored in a width pixel * height pixel node array:

typedef struct node{
	int input;
	float weight;
	float output;
} node;

During creation of the neural network inputs, weights and outputs are all zeroed out by default.

Neuron Inputs are entered via the BMP data array.
Neuron weight are initially set to 0 and updated during training
Neuron output are a multiple of weight and input.

For a single node: output = input * weight;

4.3.0 Output

Linear regression is used to generate output: https://en.wikipedia.org/wiki/Linear_regression

This is done by: Total Output = Coefficient + (weight1 * input1) + (weight2 * input2)... (weight(n) * input(n))

The coefficient is dropped because it's an unnecessary shift in this case:
	Total Output = (weight1 * input1) + (weight2 * input2)... (weight(n) * input(n))

Adding every output of every node gives us the linear regression output.

4.4.0 Adjusted Weights

During training runs (single or batch) it's possible to change the weights of the nodes.

Using a Perceptron weight update function (based on delta rule)

w = w + r * (d - y) * x

new weight = old weight + learning rate * (desired output - given output) * input

Uses the difference between the desired output against the linear regression output to adjust weight.

Because the desired output is reduced to the binary (1 for x and -1 for o), the output also needs to be reduced to the binary for the weights to adjust appropriately (positive output = 1, zero output = 0, and negative output = -1).

In freezenn.c, this function updates the weight:
void updateWeightDelta(node* neuralNet, float outputTotal, char runType)


A secondary older weight function (not used):
void updateWeights(node *neuralNet, float outputTotal, char runType )

The old function was used before using the delta rule for single layer perceptrons, this weight update function pushes the weights by a small percentage amount 1/5 when the total output don't match the desired output.

5.0.0 Conclusion

This is the most basic ANN developed very quickly based on the Elements of AI instruction. It shows the use of basic image recognition program, training an AI and its use in prediction/image recognition.

The hardest and longest part of the project was parsing, unpacking and checking the input data out of the BMP file. The neuron development was simple enough, weight and output was updated to a float value to allow for finer adjustments.

My former studies in AI was regarding expert systems which were considered weighted databases, so it was quite surprising how simple yet effective this form of processing can be. Of course, larger multi-layer AIs would add exponential difficulty in every possible way (NN traversal, storage, algorithmic difficulty)

In regards to implementation learning, keeping the implementation KISS (Keep It Simple Stupid) allows for faster results, easier debugging and learning outcomes.

Batch training is definitely the way forward and should be taught alongside AI, feeding in training information, case by case, isn't an effective learning mechanism.

Large data sets for training is a major obstacle for the lone programmer, exploitation of search engines and other online resources is a viable option. Effective training is key to an effective neural network.

With a training set of 100 files and 6 test cases, one test image still fails to predict correctly after batch training(noticable with both weighting functions). Training and testing must be representative of potential inputs, and any unrepresented or under-representation of fringe cases have the potential to cause prediction errors.

5.1.0 Improvements

Improvements would include a greater image parsing potential and unpacking the width, height and bit depth variables directly from the image file. Extending the program to cover PNG, JPEG and other common image formats.
Perhaps this can be extended with a third party image processing library, thus keeping the project on AI and not image processing.

Add choice of weight update function. Two functions are available but aren't switchable unless you rewrite code.

Print out the neural network in pixel width form. *This will be worked on before final release
command: $/freeze printw

Extending into a multi-layer Perceptron could also be an improvment.

Working on error rate output for large data sets of test cases.

5.2.0 Programming

Key functions and how they can be replaced to offer alternate functionality:

unsigned char * getBMPData(char *filename) function in filefreezenn.c returns the input array of pixels given a filename.
Replacing this function will allow for different images data inputs to be used, this function is called in the main.c file in single and batch training.

The linear regression algorithm can be changed in freezeNN.c between lines 223 and 233. As the outputs are generated and tallied.

The weight update function can be changed here, found in freezenn.c file.
void updateWeightDelta(node* neuralNet, float outputTotal, char runType)

UpdateWeightDelta is called on line 250 in freezenn.c. Updating weights is only done if the runtype is either 1 or 0, with 2 being a simple prediction with no weight update.

Batch training is done in freezenn.c, line 271.


5.3.0 Future Work

A revision of the Elements of AI to reinforce all lessons, the X and O neural net was a small section of the whole course.

The machine learning crash course by Google (Tensor Flow):
https://developers.google.com/machine-learning/crash-course/

Stanford CS class CS231n: Convolutional Neural Networks for Visual Recognition
https://cs231n.github.io/convolutional-networks/

Development of a multilayer nueral network in C++, Python or Java.

6.0.0 Appendix

Repository Location

University of Helsinki: Elements of AI
https://www.elementsofai.com/

Artificial neural network
https://en.wikipedia.org/wiki/Artificial_neural_network

Biological Neuron
https://en.wikipedia.org/wiki/Neuron

Perceptron
https://en.wikipedia.org/wiki/Perceptron

Linear Regression
https://en.wikipedia.org/wiki/Linear_regression

BMP file format
https://en.wikipedia.org/wiki/BMP_file_format

C Cheat Sheet reference - Ashlyn Black
https://www.cheatography.com/ashlyn-black/cheat-sheets/c-reference/

Delta Rule - Learning Algorithm
https://en.wikipedia.org/wiki/Delta_rule

Thanks for reading this readme