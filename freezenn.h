#ifndef FREEZENN_H
#define FREEZENN_H

//#include "freezeglobal.h"
#include "filefreezenn.h"

/*
runner for neural network, takes verified image file name
and expected output ('0','1') or '2' automatic for a normal NN prediction
if any of the input data is invalid print an error and return
*/
void runNN(unsigned char *dataArray, char runType);

//runs linear algorithm
void generateOutput(node *neuron);

//getter for returning output
//int addTotal(node *neuron);
float getOutput(node *neuron);

//Train NN using batch data
void batchTrain();


#endif