build : main.o freezenn.o filefreezenn.o
	gcc -Wall -o freeze main.o freezenn.o filefreezenn.o
	
main.o : freezeglobal.h freezenn.h filefreezenn.h
	gcc -Wall -c main.c
freezenn.o : freezeglobal.h freezenn.h filefreezenn.h
	gcc -Wall -c freezenn.c
filefreezenn.o : freezeglobal.h filefreezenn.h
	gcc -Wall -c filefreezenn.c

clean :
	del main.o freezenn.o filefreezenn.o freeze.exe
	
dnn :
	del freezeNN.dat
	
setup :
	mkdir train
	
