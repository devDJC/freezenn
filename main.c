/**
*A basic neural net program based on the Uni. of Helsinki's Elements of AI neural network example
*FreezeNN
*Written by: David Clarke 2018
*
*predicts monochrome 10x10pixels X or O images
*uses batch training data
*
*first program will be simple perceptatron (linear regression) (completed)
*second program will feature back propagation algotrithm 
*third program will perhaps go further than simple 10x10 pixel images (completed*)
*(*)Image can be of any size (set in setup), but it needs to be a monochrome bmp file or the bmp loader won't work
*The pixel data array is 1 for black 0 for white.
*/


#include "freezenn.h"
//#include "filefreezenn.h"

//DEBUG SWITCH
#define DEBUG 0

/**
*function usageHelp
*
*prints usage information
*
*update with html information?
*/
void usageHelp(void){
	printf("\nHelp Dialog: Thank you for using this program, readme file has setup information\n\n");
	printf("This program creates and trains a simple perceptron neural network based upon the Uni. Helsinki: Elements of AI\n\n");
	printf("To dispaly this message\nUsage: programName\nor: programName -h\n\n");
	printf("Prediction\nImage Prediction 10px x 10px BMP image.\nUsage: programName ImageName\n\n");
	printf("Training\nImage Training 10px x 10px BMP image. \nUsage: programName ImageName expectedOutcome\n");
	printf("Expected outcome flag is 0 for O shape and 1 for X shape image.\n");
	printf("*Warning* Expected outcome flag must match image type or training will fail\n\n");
	printf("Print Neural Network\nUsage: programName print\n\n");
	printf("Print Neural Network weights in pixel width rows\nUsage: programName printw\n\n");
	printf("Batch Training\nUsage: programName batch\n");
}

/**
*function main
*
*main program function, parses argv and runs the program
*returns 0 or 1 for errors.
*
*@param argc command line argument
*@param argv[] an array of pointers to strings
*@return int status
*/
int main(int argc, char *argv[]){
	unsigned char *pixelArray = NULL; //input data
	
	
	//used to test system byte sizes
	if(DEBUG){
		printf("char %d\n", sizeof(char));
		printf("unsigned char %d\n", sizeof(unsigned char));
		printf("int %d\n", sizeof(int));
		printf("double %d\n", sizeof(double));
		printf("long %d\n", sizeof(long));	
	}
	
	//check setupfile and load enviroment and image data
	if(checkFile(SETUP_FILENAME)){
		if(!loadSetup(SETUP_FILENAME)){
			printf("Error: failed reading setup.txt file, check setup.txt formatting\n");
			return 1;
		}	
	}
	else{
		printf("Error: setup.txt doesn't exist read readme file\n");
		return 1;
	}
	
	
	//on the command line imports an image file and parses pixel information
	if(argc == 1){
		usageHelp();
		return 0;
	}
	else if (argc == 2){
		//normal usage prediction or help
		if(strncmp(argv[argc-1], "-h", 2) == 0) //trigger help menu
		{
			usageHelp();
			return 0;
		}
		else if(strncmp(argv[argc-1], "printw", 6) == 0){ //trigger print output of the NN
			
			printNNW();
			
		}
		else if(strncmp(argv[argc-1], "print", 5) == 0){ //trigger print output of the NN
			
			printNN();
			//load nn
			//output nn		
		}
		else if((strncmp(argv[argc-1], "batch", 5) == 0)){ //trigger training batch based on setup file
			//batch process
			batchTrain();
			//return 0;
		}
		else if(checkFile(argv[argc-1])){  //check file exists
			pixelArray = getBMPData(argv[argc-1]); //pixel imputs feed into basic nn
			
			if(pixelArray != NULL){
				runNN(pixelArray, '2'); //run prediction on image filename
				free(pixelArray);
			}
			
		}
		else{
			return 0;
		}
	}
	else if (argc == 3){ //training run with expected output
		
		if(*argv[argc-1] == '0' || *argv[argc-1] == '1'){
			if(checkFile(argv[argc-2])) //check file exists
			{
				pixelArray = getBMPData(argv[argc-2]); //pixel imputs feed into basic nn
				if(pixelArray!= NULL){
					runNN(pixelArray, *argv[argc-1]); //run training model with expected outcome
					free(pixelArray);
				}
				
			}
		}
		else{
			printf("Error: Expected outcome for training data not 0 (o) or 1 (x)\n");
			usageHelp();
			return 1;
		}
		
		
	}
	else if (argc > 3){
		usageHelp();
	}

	free(NN_FILENAME);
	free(BATCH_FILENAME);
	free(BATCH_LOCATION);
	
	return 0;
}