# FreezeNN

After completing the University of Helsinki's: Elements of AI free online course https://www.elementsofai.com/. I decided to implement a basic neural network, Freeze(FreezeNN), based on their example of an image recognition neural network.