#ifndef FREEZEGLOBAL_H
#define FREEZEGLOBAL_H

#include<stdio.h>
#include<string.h>
#include<stdlib.h>

#define SETUP_FILENAME "setup.txt"
//#define NNFILENAME "freezeNN.dat"
//#define DATASIZE 100 //size of 10x10 pixel array
//will need to make this dynamic for larger network
//char *NN_FILENAME = NULL;
//unsigned int DATA_SIZE = 0;
//unsigned int PIXEL_WIDTH = 0;
//unsigned int PIXEL_HEIGHT = 0;
//char NN_FILENAME[25];
/**
*Setup file global variables
*
*neural net filename
*batch file filename
*batch directory
*size of batch directory name for memory allocation
*data size = pixel height * pixel width
*pixel height from setup file
*pixel width from setup file
*
*/
char *NN_FILENAME;
char *BATCH_FILENAME;
char *BATCH_LOCATION;
unsigned int LOCATION_SIZE;

unsigned int DATA_SIZE;
unsigned int PIXEL_WIDTH;
unsigned int PIXEL_HEIGHT;


typedef enum bool{false, true} bool; //user defined bool type

//basic 1 to 1 node
//needs to be turned into n-node array
/*
typedef struct node{
	int input;
	int weight;
	int output;
} node;
*/

//neural net structure
typedef struct node{
	int input;
	float weight;
	float output;
} node;


#endif